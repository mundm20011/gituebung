# Branches

Ein Branch in Git ist einfach ein leichter, beweglicher Zeiger auf einen dieser Commits. Die Standardbezeichnung für einen Branch bei Git lautet master . Wenn Sie damit beginnen, Commits durchzuführen, erhalten Sie einen master Branch, der auf den letzten Commit zeigt, den Sie gemacht haben.

![Beispiel Branch](Branch.png)
