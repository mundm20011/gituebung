# GitUebung

**Git**
Git ist ein ausgereiftes, aktiv gepflegtes Open-Source-Projekt, das 2005 ursprünglich von Linus Torvalds, dem berühmten Entwickler des Linux Betriebssystem-Kernel, entwickelt wurde.

```
git clone https://gitlab.com/mundm20011/gituebung.git
git pull
git add .
git commit -m "nachricht"
git push
git merge
git fetch
git --help
git status
```

 Git ist ein Open-Source System zur verteilten Versionierung. Eingesetzt wird es für die Versionskontrolle (Protokollierung von Anpassungen) von Dateien.

 [Was sind Branches](branches.md)

 [Nützliche links](links.md)

 **Review**
 **Tim Kral**
 **24.11.2023**